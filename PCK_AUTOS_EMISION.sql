create or replace PACKAGE BODY PCK_AUTOS_EMISION IS
  /**
  @Nombre          P_GENERAR_POLIZA
  @Descripcion     Proceso emision polizas de autos para el agrupador Banco Rio.
  @Aplicacion      POLARIS_WINDOWS
  @Modulo          Modulo
  @Funcion         Funcion
  @Tipo            Logica Negocios
  @Retorno         --
  @Autor           MARTINEZR
  @Fecha_Creacion  23/11/2005
  @Palabras_Claves emision polizas autos banco rio
  @Reusabilidad    Baja
  @Activo          S
  
  @Param_Nombre    pn_cod_cia
  @Param_Descrip   corresponde al codigo de cia
  @Param_Nombre    pn_nro_presup
  @Param_Descrip   numero de presupuesto
  @Param_Nombre    pn_verif_propiedad_veh
  @Param_Descrip   verif_propiedad_veh de emi_prod_autos
  @Param_Nombre    pn_estado_general
  @Param_Descrip   estado_general de emi_prod_autos
  @Param_Nombre    pn_tipo_vidrio_tonalizado
  @Param_Descrip   tipo_vidrio_tonalizado de emi_prod_autos
  @Param_Nombre    pv_cristales_grabados
  @Param_Descrip   cristales_grabados de emi_prod_autos
  @Param_Nombre    pv_tipo_defensa_esp
  @Param_Descrip   tipo_defensa_esp de emi_prod_autos
  @Param_Nombre    pn_kms_momento_aseg
  @Param_Descrip   kms_momento_aseg de emi_prod_autos
  @Param_Nombre    pv_acred_prend
  @Param_Descrip   acred_prend de emi_prod_autos
  @Param_Nombre    pn_num_pol1
  @Param_Descrip   numero de poliza devuelto
  @Param_Nombre    pv_mensaje_error
  @Param_Descrip   mensaje de error (si hay) devuelto
  @Param_Nombre    pv_retorno
  @Param_Descrip   parametro de salida (error/success)
  @Param_Nombre    pn_cod_secc
  @Param_Descrip   corresponde al codigo de seccion, default 4
  @Param_Nombre    pn_cod_ramo
  @Param_Descrip   corresponde al codigo de ramo, default 1
  
  --@Mod_Fecha       05/05/2006
  --@Mod_SAR         P000093904
  --@Mod_Autor       MATIAS de TORO
  --@Mod_Descripcion Determino el proveedor antes de llamar a p_emi_genero_poliza
  --                 polaris.
  
  */
  PROCEDURE P_GENERAR_POLIZA(PN_COD_CIA                IN NUMBER,
                             PN_COD_SECC               IN NUMBER, --RCM
                             PN_COD_RAMO               IN NUMBER, --RCM
                             PN_NRO_PRESUP             IN NUMBER,
                             PN_VERIF_PROPIEDAD_VEH    IN NUMBER,
                             PN_ESTADO_GENERAL         IN NUMBER,
                             PN_TIPO_VIDRIO_TONALIZADO IN NUMBER,
                             PV_CRISTALES_GRABADOS     IN VARCHAR2,
                             PV_TIPO_DEFENSA_ESP       IN VARCHAR2,
                             PN_KMS_MOMENTO_ASEG       IN NUMBER,
                             PV_ACRED_PREND            IN VARCHAR2,
                             PN_NUM_POL1               OUT NUMBER,
                             PV_MENSAJE_ERROR          OUT VARCHAR2,
                             PV_RETORNO                OUT VARCHAR2,
                             P_CANT_MENSUALIDADES      OUT NUMBER /*,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 --agregado German Cruz (Banco Rio)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 pn_cant_cuotas              OUT      NUMBER,       -- milen026 14/11/2006
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 pc_cuotas                   IN OUT  ccuotas       -- milen026 14/11/2006
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               */) IS
    P_AGENCIA  NUMBER := NULL;
    P_DURACION NUMBER := NULL;
    --p_poliza_prov VARCHAR2(2)      := NULL;
    P_POLIZA           NUMBER := NULL;
    P_COD_PROD         NUMBER := NULL;
    P_ID               NUMBER := NULL;
    P_ID_RIESGO        NUMBER := NULL;
    P_TELEFONO         VARCHAR2(20) := NULL;
    P_FECHAVIG         DATE := NULL;
    PL_0KM             VARCHAR2(2) := NULL;
    PN_COD_PLAN        NUMBER := NULL;
    LN_NUM_POL         NUMBER := NULL;
    LN_CANT_RENOV      NUMBER := NULL;
    LN_NUMPOL_RENOV    NUMBER := NULL;
    LN_NUM_POL_FACTURA NUMBER := NULL;
    P_ERROR            VARCHAR2(300) := NULL;
    PN_CANTCUOTAS      NUMBER := NULL;
    P_MODELO_EQUIPO    EMI_PROD_AUTOS.MODELO_EQUIPO%TYPE := NULL;
    P_PROVEEDOR_EQUIPO EMI_PROD_AUTOS.PROVEEDOR_EQUIPO%TYPE := NULL;
    P_COD_INICIADOR    EMI_PRESU_SOLIC.COD_INICIADOR%TYPE := NULL;
    P_FOR_COBRO        EMI_PRESU_SOLIC.FOR_COBRO%TYPE := NULL;
    /*agregado German Cruz (Banco Rio)*/
    V_PARA    VARCHAR2(1000);
    V_CC      VARCHAR2(1000);
    V_CCO     VARCHAR2(1000);
    V_MENSAJE VARCHAR2(200);
    V_ERROR   VARCHAR2(1000) := '';
    /*fin agregado German Cruz (Banco Rio)*/
    VMCA_VIP   VARCHAR2(2) := 'N';
    NSUMA_ASEG NUMBER(17, 2);
    VCOD_TIPO  VARCHAR2(2);
    -----------------------------
    -- MILEN026 14/11/2006
    C1           CCUOTAS;
    REC          RCUOTAS;
    CUOTAS       RCUOTAS;
    PN_CANTRENOV A1000754.CANTIDAD_RENOV%TYPE;
  
    -------------------------------------
    CURSOR CAGENCIA IS
      SELECT COD_AGENCIA
        FROM G1002700
       WHERE COD_USER_CIA = REPLACE(USER, 'OPS$');
  
    -- RIO PRENDARIO  COLVI 12/10/2006
    LB_EXISTE_A1000757 BOOLEAN := FALSE;
    LN_RENOVACIONES    NUMBER := 0;
  
    -- 176853 - Milen026 27/04/2009: se crea el cursor c_auto para obtener 
    -- los datos del auto para enviar a pck_autos_solic2.equipo_anterior
    CURSOR C_AUTO IS
      SELECT PAT_VEH,
             MOTOR_VEH,
             CHASIS_VEH,
             NRO_ANU_POL,
             ID,
             COD_MOD,
             COD_PLAN,
             COD_INICIADOR,
             COD_ESTADO, -- cyudowsky - req. 179401
             nro_equipo -- 184925
            ,
             b.negocio -- 187461 koala007 27/06/2013
            ,
             b.cod_prod -- ITDES-217452  Degli
        FROM EMI_PROD_AUTOS A, EMI_PRESU_SOLIC B
       WHERE A.NRO_PRESUP = PN_NRO_PRESUP
         AND A.NRO_PRESUP = B.NRO_PRESUP;
    PR_AUTO C_AUTO%ROWTYPE;
    
    -- ITDES-221614 - Negocios Prendarios | ASAP | Agosto 2016    
    CURSOR c_num_secu_pol(p_cia in number, p_ramo in number, p_poliza in number) IS
       SELECT num_secu_pol, fecha_venc_pol
         FROM a2000030 a
        WHERE cod_cia = p_cia
          AND cod_ramo = p_ramo
          AND num_pol1 = p_poliza
          AND num_end = 0;              
    pl_num_secu_pol    NUMBER := 0;
  
    LV_NRO_EQUIPO      EMI_PROD_AUTOS.NRO_EQUIPO%TYPE := NULL;
    LD_INSTAL_EQUIPO   EMI_PROD_AUTOS.FECHA_INSTAL_EQUIPO%TYPE := NULL;
    LV_MOD_EQUIPO      EMI_PROD_AUTOS.MODELO_EQUIPO%TYPE := NULL;
    LN_PROVEED_EQUIPO  EMI_PROD_AUTOS.PROVEEDOR_EQUIPO%TYPE := NULL;
    LN_NSP_CONT_EQ     EMI_PROD_AUTOS.NSP_CONT_EQ%TYPE := NULL;
    LN_CRIES_CONT_EQ   EMI_PROD_AUTOS.CRIES_CONT_EQ%TYPE := NULL;
    LV_MSG_EQUIPO      VARCHAR2(1) := NULL;
    LN_CANT_RENOV_REAL NUMBER := NULL;
    LV_VALIDADO        VARCHAR2(1) := NULL;
    LV_MENSAJE_ERROR   VARCHAR2(100) := NULL;
    LV_MODALIDAD       VARCHAR2(1) := NULL;
    
    pl_fecha_venc_pol  date;
    pv_cod_error       number;

    nfranq      emi_premio_x_plan.imp_franquicia%type; --itdes-240084
    nfranq_robo emi_premio_x_plan.imp_franquicia_robo%type;--itdes-240084
    
  BEGIN
  
    PV_RETORNO       := NULL;
    PV_MENSAJE_ERROR := NULL;
  
    -- Recupero la Agencia del usuario
    OPEN CAGENCIA;
  
    FETCH CAGENCIA
      INTO P_AGENCIA;
  
    CLOSE CAGENCIA;
  
    IF P_AGENCIA IS NULL THEN
      PV_RETORNO := CONSTANTES.ERROR;
      --Asigno el retorno especificado para error
      PV_MENSAJE_ERROR := 'El usuario no tiene agencia asignada.';
      RETURN; -- Sale del proceso si hubo error al validar los datos
    END IF;
  
    /*--- Recupero marca 0km ---*/
    BEGIN
      SELECT NVL(A.MCA_CERO_KM, 'NO'),
             A.COD_PLAN,
             NVL(A.SUMA_ASEG, 0),
             B.COD_TIPO
        INTO PL_0KM, PN_COD_PLAN, NSUMA_ASEG, VCOD_TIPO
        FROM EMI_PROD_AUTOS A, A1040400 B
       WHERE A.COD_CIA = PN_COD_CIA
         AND A.NRO_PRESUP = PN_NRO_PRESUP
         AND B.COD_CIA = A.COD_CIA
         AND B.COD_MARCA = A.COD_MARCA
         AND ROWNUM = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        PL_0KM := 'NO';
      WHEN OTHERS THEN
        NULL;
    END;
  
    -- Verificar y validar campos de entrada  ---
    VALIDACION_DATOS(PN_VERIF_PROPIEDAD_VEH,
                     PN_COD_CIA,
                     PN_ESTADO_GENERAL,
                     PN_TIPO_VIDRIO_TONALIZADO,
                     PV_CRISTALES_GRABADOS,
                     PV_TIPO_DEFENSA_ESP,
                     PN_KMS_MOMENTO_ASEG,
                     PV_ACRED_PREND,
                     PL_0KM,
                     PV_MENSAJE_ERROR);
  
    IF PV_MENSAJE_ERROR IS NOT NULL THEN
      PV_RETORNO := CONSTANTES.ERROR;
      --Asigno el retorno especificado para error
      RETURN; -- Sale del proceso si hubo error al validar los datos
    END IF;
  
    -- Matias de Toro 05/05/2006 req.P000093904: validar los datos del equipo
    --- Recupera parametros necesarios ---
    RECUPERA_PARAMETROS(PN_COD_CIA,
                        PN_COD_SECC,
                        PN_COD_RAMO,
                        PN_NRO_PRESUP,
                        P_COD_PROD,
                        P_ID,
                        P_ID_RIESGO,
                        P_TELEFONO,
                        P_FECHAVIG,
                        PV_MENSAJE_ERROR);
  
    IF PV_MENSAJE_ERROR IS NOT NULL THEN
      PV_RETORNO := CONSTANTES.ERROR;
      RETURN;
    END IF;

    /*--ITDES-240084 - Recupero datos de franquicia ---*/
    BEGIN
       SELECT imp_franquicia, 
              imp_franquicia_robo
         INTO nfranq, 
              nfranq_robo
         FROM emi_premio_x_plan a, 
              emi_prod_autos b
        WHERE a.nro_presup = pn_nro_presup
          AND b.nro_presup = a.nro_presup
          AND a.cod_plan = b.cod_plan; 
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        nfranq := null; 
        nfranq_robo := null;
      WHEN OTHERS THEN
        NULL;
    END;    
    --ITDES-240084 fin 
  
    -- Recuperador para insertar los correctos --
    P_RECUPERA_DATOS_EQUIPO_RECU(PN_COD_CIA,
                                 PN_COD_SECC,
                                 PN_COD_RAMO,
                                 PN_NRO_PRESUP,
                                 P_COD_PROD,
                                 P_FECHAVIG,
                                 P_ERROR,
                                 P_MODELO_EQUIPO,
                                 P_PROVEEDOR_EQUIPO);
  
    IF P_ERROR IS NOT NULL THEN
      PV_RETORNO := CONSTANTES.ERROR;
      --Asigno el retorno especificado para error
      PV_MENSAJE_ERROR := 'Error al recuperar datos del equipo - ' ||
                          P_ERROR;
      RETURN; -- Sale del procedure si hubo error al validar los datos
    END IF;
  
    -- 176853 - Milen026 30/04/2009: agregamos la condicion de Proveedor Ituran ya que
    -- para casos LoJack esta dando error al insertar en p_itu_agenda_inspecciones.
    -- 176853 - Testeo-1277: se modifica el IF para que invoque a equipo_anterior
    -- en caso de que el proveedor no sea nulo.
  
    PR_AUTO := NULL;
    OPEN C_AUTO;
    FETCH C_AUTO
      INTO PR_AUTO;
    CLOSE C_AUTO;
  
    IF P_PROVEEDOR_EQUIPO IS NOT NULL AND
      -- 184925 Se evalua continuidad si no tiene equipo cargado en la solicitud
       pr_auto.nro_equipo is null THEN
      -- 176853 - Milen026 27/04/2009: si el proveedor_equipo es no nulo
      -- se llama a pck_autos_solic2.equipo_anterior para recuperar los datos
      -- del equipo, mas especificamente, el modelo que es el campo que se actualiza
    
      PCK_AUTOS_SOLIC2.EQUIPO_ANTERIOR(P_PROVEEDOR_EQUIPO,
                                       P_MODELO_EQUIPO,
                                       PN_NRO_PRESUP,
                                       PN_COD_CIA,
                                       PN_COD_SECC,
                                       PN_COD_RAMO,
                                       PR_AUTO.NRO_ANU_POL,
                                       PR_AUTO.PAT_VEH,
                                       PR_AUTO.MOTOR_VEH,
                                       PR_AUTO.CHASIS_VEH,
                                       PR_AUTO.ID,
                                       LV_NRO_EQUIPO,
                                       LD_INSTAL_EQUIPO,
                                       LV_MOD_EQUIPO,
                                       LN_PROVEED_EQUIPO,
                                       LN_NSP_CONT_EQ,
                                       LN_CRIES_CONT_EQ);
    
    END IF;
  
    VMCA_VIP := PCK_PRE0030.F_VALIDA_MCA_VIP(PN_COD_CIA,
                                             PN_NRO_PRESUP,
                                             PN_COD_PLAN,
                                             NSUMA_ASEG,
                                             VCOD_TIPO);
  
    /* req. 179401 - cyudowsky - inicio */
    PCK_AUTOS_SOLIC.P_EMISION_FLEXIBILIZADA(PR_AUTO.COD_MOD,
                                            PR_AUTO.COD_PLAN,
                                            PR_AUTO.COD_INICIADOR,
                                            PL_0KM,
                                            LV_VALIDADO,
                                            LV_MENSAJE_ERROR,
                                            PN_COD_CIA,
                                            PN_COD_RAMO,
                                            PN_NRO_PRESUP,
                                            LV_MODALIDAD);
  
    IF LV_VALIDADO = 'S' AND PR_AUTO.COD_ESTADO IS NOT NULL THEN
      UPDATE EMI_PROD_AUTOS
         SET VERIF_PROPIEDAD_VEH    = PN_VERIF_PROPIEDAD_VEH,
             ESTADO_GENERAL         = PN_ESTADO_GENERAL,
             TIPO_VIDRIO_TONALIZADO = PN_TIPO_VIDRIO_TONALIZADO,
             CRISTALES_GRABADOS     = PV_CRISTALES_GRABADOS,
             TIPO_DEFENSA_ESP       = PV_TIPO_DEFENSA_ESP,
             KMS_MOMENTO_ASEG       = PN_KMS_MOMENTO_ASEG,
             ACRED_PREND            = PV_ACRED_PREND,
             NRO_EQUIPO             = NVL(LV_NRO_EQUIPO, NRO_EQUIPO),
             FECHA_INSTAL_EQUIPO    = NVL(LD_INSTAL_EQUIPO,
                                          FECHA_INSTAL_EQUIPO),
             MODELO_EQUIPO          = NVL(P_MODELO_EQUIPO, LV_MOD_EQUIPO),
             PROVEEDOR_EQUIPO       = P_PROVEEDOR_EQUIPO,
             NSP_CONT_EQ            = LN_NSP_CONT_EQ,
             CRIES_CONT_EQ          = LN_CRIES_CONT_EQ,
             MCA_VIP                = VMCA_VIP,
             COD_ESTADO             = NULL, -- req. 179401 - cyudowsky
             val_franq              = nfranq, --ITDES-240084
             val_franq_robo         = nfranq_robo --ITDES-240084
       WHERE COD_CIA = PN_COD_CIA
         AND NRO_PRESUP = PN_NRO_PRESUP;
    ELSE
    
      /* req. 179401 - cyudowsky fin */
    
      -- 176853 - Testeo-1277: se agregan los campos devueltos por equipo_anterior
      -- para que se actualicen en la tabla emi_prod_autos
    
      -- Si se validaron OK entonces se actualiza la tabla EMI_PROD_AUTOS--
    
      UPDATE EMI_PROD_AUTOS
         SET VERIF_PROPIEDAD_VEH    = PN_VERIF_PROPIEDAD_VEH,
             ESTADO_GENERAL         = PN_ESTADO_GENERAL,
             TIPO_VIDRIO_TONALIZADO = PN_TIPO_VIDRIO_TONALIZADO,
             CRISTALES_GRABADOS     = PV_CRISTALES_GRABADOS,
             TIPO_DEFENSA_ESP       = PV_TIPO_DEFENSA_ESP,
             KMS_MOMENTO_ASEG       = PN_KMS_MOMENTO_ASEG,
             ACRED_PREND            = PV_ACRED_PREND,
             NRO_EQUIPO             = NVL(LV_NRO_EQUIPO, NRO_EQUIPO),
             FECHA_INSTAL_EQUIPO    = NVL(LD_INSTAL_EQUIPO,
                                          FECHA_INSTAL_EQUIPO),
             MODELO_EQUIPO          = NVL(P_MODELO_EQUIPO, LV_MOD_EQUIPO),
             PROVEEDOR_EQUIPO       = P_PROVEEDOR_EQUIPO,
             NSP_CONT_EQ            = LN_NSP_CONT_EQ,
             CRIES_CONT_EQ          = LN_CRIES_CONT_EQ,
             MCA_VIP                = VMCA_VIP,
             val_franq              = nfranq, --ITDES-240084
             val_franq_robo         = nfranq_robo --ITDES-240084             
       WHERE COD_CIA = PN_COD_CIA
         AND NRO_PRESUP = PN_NRO_PRESUP;
    END IF;
  
    IF SQL%ROWCOUNT = 0 THEN
      PV_RETORNO       := CONSTANTES.ERROR;
      PV_MENSAJE_ERROR := 'Error al actualizar EMI_PROD_AUTOS - ' ||
                          SQLERRM;
      RETURN;
    END IF;
  
    -- Ejecuta controles tecnicos --
    CONTROLES_TECNICOS(PN_NRO_PRESUP,
                       PN_COD_CIA,
                       PN_COD_SECC,
                       PN_COD_RAMO,
                       P_AGENCIA,
                       PV_MENSAJE_ERROR);
  
    IF PV_MENSAJE_ERROR IS NOT NULL THEN
    
      -- ITDES-217452 - BBVA Degli      
      -- Recuperamos si el agrupador trabaja con mensajes
      -- Si trabaja tradusco mensaje
      -- Si no trabaja me quedo con el mensaje genérico
      if pck_ws_errores.P_MCA_WS_ERROR_X_AGRUPADOR(P_COD_AGRUPADOR => pr_auto.cod_iniciador) = 'S' then
        declare
          aux_error varchar2(2000) := PV_MENSAJE_ERROR;
        begin
          pck_ws_errores.P_MENSAJE_X_AGRUPADOR(P_COD_PROD      => pr_auto.cod_prod,
                                               P_ERROR         => aux_error,
                                               P_ERROR_RETORNO => PV_MENSAJE_ERROR);
        exception
          when others then
            PV_MENSAJE_ERROR := 'Operación no aceptada';
        end;
      else
        PV_MENSAJE_ERROR := 'Operación no aceptada';
      end if;
      -- Fin cambios ITDES-217452 - BBVA Degli
      --
      --agregado German Cruz (Banco Rio)
      PV_RETORNO := CONSTANTES.ERROR;
      --Asigno el retorno especificado para error
      RETURN; -- Sale del procedure si hubo error al validar los datos
    END IF;
  
    -- Recupero duracion --
    BEGIN
      SELECT COD_DURACION,
             COD_INICIADOR, --milen026 14/11/2006
             FOR_COBRO --milen026 14/11/2006
        INTO P_DURACION, P_COD_INICIADOR, P_FOR_COBRO
        FROM EMI_PRESU_SOLIC
       WHERE COD_CIA = PN_COD_CIA
         AND COD_SECC = PN_COD_SECC
         AND COD_RAMO = PN_COD_RAMO
         AND NRO_PRESUP = PN_NRO_PRESUP
         AND ROWNUM = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        P_DURACION      := NULL;
        P_COD_INICIADOR := NULL;
        P_FOR_COBRO     := NULL;
      
      WHEN OTHERS THEN
        NULL;
    END;
  
    -- Se deben marcar de nuevo los agravantes
    PCK_AGRAVANTE.ARMAR_AGRAVANTES(PN_NRO_PRESUP,
                                   1, -- cod_ries
                                   PN_COD_CIA,
                                   PN_COD_SECC,
                                   PN_COD_RAMO);
    -- Llamada a P_EMI_GENERO_POLIZA_POLARIS --
    P_EMI_GENERO_POLIZA_POLARIS(PN_COD_CIA,
                                PN_COD_SECC,
                                PN_COD_RAMO,
                                PN_NRO_PRESUP,
                                P_DURACION,
                                'NO',
                                P_POLIZA -- IN OUT
                                );
  
    IF P_POLIZA IS NOT NULL THEN
      PN_NUM_POL1 := P_POLIZA;
    ELSE
      PV_RETORNO       := CONSTANTES.ERROR;
      PV_MENSAJE_ERROR := 'Error al generar poliza(P_EMI_GENERO_POLIZA_POLARIS) - ' ||
                          SQLERRM;
      RETURN;
    END IF;
  
    -- Llamado al facturador (COBOL) --
    EJECUTA_COBOL(PN_COD_CIA,
                  PN_COD_SECC,
                  PN_COD_RAMO,
                  PN_NUM_POL1,
                  0,
                  
                  -- num_end
                  PV_MENSAJE_ERROR);
  
    IF PV_MENSAJE_ERROR IS NOT NULL THEN
      PV_RETORNO := CONSTANTES.ERROR;
      RETURN;
    END IF;
  
    --176447 TESTEO-1106
    -- Milen018 09-03-2009 se invoca a la nueva función f_obtener_cant_renov_pol_ljm
  
    -- 178362 Milen018
    -- 09-11-2009: Se modifica el desarrollo para la renovación, queda como lo realiza el pre0080.
  
    LN_NUM_POL           := PN_NUM_POL1;
    P_CANT_MENSUALIDADES := 1;
  
    P_RENUEVA_EMISION(PN_COD_CIA  => PN_COD_CIA,
                      PN_COD_SECC => PN_COD_SECC,
                      PN_COD_RAMO => PN_COD_RAMO,
                      PN_NUM_POL1 => PN_NUM_POL1,
                      PV_MSG      => LV_MSG_EQUIPO);
  
    IF LV_MSG_EQUIPO IS NULL THEN
     -- ITDES-221614 - Negocios Prendarios | ASAP | Agosto 2016
     pl_num_secu_pol := null;
     OPEN c_num_secu_pol(pn_cod_cia, pn_cod_ramo, pn_num_pol1);
       FETCH c_num_secu_pol INTO pl_num_secu_pol, pl_fecha_venc_pol;
     CLOSE c_num_secu_pol;
     
     IF pck_agrupadores2.pp_sin_instal(pl_num_secu_pol) = 'S' THEN 
      LN_CANT_RENOV_REAL :=  f_obtener_cant_renov_pol_pp(P_COD_CIA  => PN_COD_CIA,
                                                        P_COD_RAMO => PN_COD_RAMO,
                                                        P_NUM_POL1 => PN_NUM_POL1,
                                                        P_COD_RIES => 1);
     ELSE
      LN_CANT_RENOV_REAL := F_OBTENER_CANT_RENOV_POL_LJM(P_COD_CIA  => PN_COD_CIA,
                                                         P_COD_RAMO => PN_COD_RAMO,
                                                         P_NUM_POL1 => PN_NUM_POL1,
                                                         P_COD_RIES => 1);
      IF LN_CANT_RENOV_REAL = 0 THEN
        -- ITDES-220384 - Modificar plazo de instalación ER Ituran | ASAP | Mayo 2016
        LN_CANT_RENOV_REAL := F_OBTENER_CANT_RENOV_POL_ITU(P_COD_CIA  => PN_COD_CIA,
                                                           P_COD_RAMO => PN_COD_RAMO,
                                                           P_NUM_POL1 => PN_NUM_POL1,
                                                           P_COD_RIES => 1);
                                                                 
        IF LN_CANT_RENOV_REAL = 0 THEN
          -- ITDES-236446 - Proveedor CEABS - Se agrega proveedor | ASAP | Marzo 2018
          LN_CANT_RENOV_REAL := F_OBTENER_CANT_RENOV_POL_PROV(P_COD_CIA  => PN_COD_CIA,
                                                           P_COD_RAMO => PN_COD_RAMO,
                                                           P_NUM_POL1 => PN_NUM_POL1,
                                                           P_COD_RIES => 1);
          
          IF LN_CANT_RENOV_REAL = 0 THEN
            LN_CANT_RENOV := F_OBTENER_CANT_RENOV_POL(P_COD_CIA  => PN_COD_CIA,
                                                      P_COD_SECC => PN_COD_SECC,
                                                      P_COD_RAMO => PN_COD_RAMO,
                                                      P_NUM_POL1 => PN_NUM_POL1,
                                                      P_COD_RIES => 1);
        
            LN_CANT_RENOV_REAL := LN_CANT_RENOV - 1;
          END IF;
        END IF;
       END IF;
      END IF;
    
      -- ITDES-239290 | Polizas emitidas en Earnix solo mensualidad 1 - WS | ASAP | 19.06.2018
      IF PCK_EARNIX_GEN.f_es_earnix(pl_num_secu_pol) = 'S' THEN
        IF nvl(ln_cant_renov_real, 0) > 0 THEN      
          pck_earnix_rv_emision.Renovar_Emision(PN_COD_CIA,
                                                PN_COD_RAMO,
                                                LN_NUM_POL,
                                                pl_fecha_venc_pol,
                                                ln_cant_renov_real,
                                                pv_cod_error,
                                                PV_MENSAJE_ERROR);
        
          IF PV_MENSAJE_ERROR is not null THEN
            pck_batch.log('EMISION WS',
                'EARNIX - Error al Renovar Póliza' || pr_auto.cod_iniciador || ';' ||
                pr_auto.negocio || ';' || TO_CHAR(LN_NUM_POL) || '; Cod:' ||
                pv_cod_error||' - Msg:'||PV_MENSAJE_ERROR);
              
            PV_MENSAJE_ERROR := NULL;
            PV_RETORNO       := CONSTANTES.SUCCESS;
            RETURN;
          END IF;
        END IF;
      ELSE      
        FOR N IN 1 .. LN_CANT_RENOV_REAL LOOP
        
          PCK_PRESTAMO.P_GENERAR_RENOVACION_PRESTAMO(PN_COD_CIA,
                                                     PN_COD_SECC,
                                                     PN_COD_RAMO,
                                                     LN_NUM_POL,
                                                     PV_MENSAJE_ERROR);
        
          -- Recupera el numero de poliza renovada para facturar
          LN_NUMPOL_RENOV := PCK_PRESTAMO.F_RECU_NUM_POL_RENOV(PN_COD_CIA,
                                                               PN_COD_SECC,
                                                               PN_COD_RAMO,
                                                               LN_NUM_POL);
        
          IF LN_NUMPOL_RENOV IS NULL THEN
            /* comentado req 187461 koala007 27/06/2013 inicio
            PV_RETORNO       := CONSTANTES.ERROR;
            PV_MENSAJE_ERROR := 'Error al Renovar Poliza : ' ||
                                TO_CHAR(LN_NUM_POL) || PV_MENSAJE_ERROR ||
                                ' , Verifique la Poliza  .';
            
            RETURN;
            */
            -- req 187461 koala007 27/06/2013 inicio
            pck_batch.log('EMISION WS',
                          'Error al Renovar Póliza;' || pr_auto.cod_iniciador || ';' ||
                          pr_auto.negocio || ';' || TO_CHAR(LN_NUM_POL) || ';' ||
                          PV_MENSAJE_ERROR);
          
            PV_MENSAJE_ERROR := NULL;
            PV_RETORNO       := CONSTANTES.SUCCESS;
            RETURN;
            -- req 187461 koala007 27/06/2013 fin
          END IF;
        
          -- Proceso facturador de la poliza renovada
          PCK_PRESTAMO.P_GENERO_FACTURA_RENOV(PN_COD_CIA,
                                              PN_COD_SECC,
                                              LN_NUMPOL_RENOV);
          -- Obtener de a2990700 si facturo la renovacion
          LN_NUM_POL_FACTURA := PCK_PRE0150_RENOV.F_RECU_FACTURAS(PN_COD_CIA,
                                                                  PN_COD_RAMO,
                                                                  LN_NUMPOL_RENOV);
        
          IF LN_NUM_POL_FACTURA IS NULL THEN
            /* comentado req 187461 koala007 27/06/2013 inicio
             --agregado German Cruz (Banco Rio)
            
             PV_RETORNO       := CONSTANTES.ERROR;
             PV_MENSAJE_ERROR := 'Error al Facturar Poliza Renovada : ' ||
                                 TO_CHAR(LN_NUMPOL_RENOV) || ' , verifique.';
            
             RETURN;
            */
            -- req 187461 koala007 27/06/2013 inicio 
            pck_batch.log('EMISION WS',
                          'Error al Facturar Poliza Renovada ; ' ||
                          pr_auto.cod_iniciador || ';' || pr_auto.negocio ||
                          TO_CHAR(LN_NUMPOL_RENOV) || ';');
          
            PV_MENSAJE_ERROR := NULL;
            PV_RETORNO       := CONSTANTES.SUCCESS;
            RETURN;
            -- req 187461 koala007 27/06/2013 fin
          
          END IF;
        
          P_CANT_MENSUALIDADES := P_CANT_MENSUALIDADES + 1;
        
          --Agregado German Cruz (Banco Rio)
          -- Se reasigna para poder renovar
          LN_NUM_POL := LN_NUMPOL_RENOV;
        
        END LOOP;
      END IF;
    END IF;
  
    /*--- Si no hay errores sale con success y mensaje nulo---*/
    PV_MENSAJE_ERROR := NULL;
    PV_RETORNO       := CONSTANTES.SUCCESS;
  
  END P_GENERAR_POLIZA; ---End P_Generar_Poliza

  /**
  @Nombre          VALIDACION_DATOS
  @Descripcion     Validacion de parametros recibidos.
  @Aplicacion      POLARIS_WINDOWS
  @Modulo          Modulo
  @Funcion         Funcion
  @Tipo            Logica Negocios
  @Retorno         --
  @Autor           MARTINEZR
  @Fecha_Creacion  23/11/2005
  @Palabras_Claves validacion parametros
  @Reusabilidad    Baja
  @Activo          S
  
  @Param_Nombre    pn_verif_propiedad_veh
  @Param_Descrip   verif_propiedad_veh de emi_prod_autos
  @Param_Nombre    pn_estado_general
  @Param_Descrip   estado_general de emi_prod_autos
  @Param_Nombre    pn_tipo_vidrio_tonalizado
  @Param_Descrip   tipo_vidrio_tonalizado de emi_prod_autos
  @Param_Nombre    pv_cristales_grabados
  @Param_Descrip   cristales_grabados de emi_prod_autos
  @Param_Nombre    pv_tipo_defensa_esp
  @Param_Descrip   tipo_defensa_esp de emi_prod_autos
  @Param_Nombre    pn_kms_momento_aseg
  @Param_Descrip   kms_momento_aseg de emi_prod_autos
  @Param_Nombre    pv_acred_prend
  @Param_Descrip   acred_prend de emi_prod_autos
  @Param_Nombre    p_cero_km
  @Param_Descrip   Marca de 0 km
  @Param_Nombre    p_mensaje
  @Param_Descrip   mensaje de error (si hay) devuelto
  
  */
  PROCEDURE VALIDACION_DATOS(PN_VERIF_PROPIEDAD_VEH    IN NUMBER,
                             PN_COD_CIA                IN NUMBER,
                             PN_ESTADO_GENERAL         IN NUMBER,
                             PN_TIPO_VIDRIO_TONALIZADO IN NUMBER,
                             PV_CRISTALES_GRABADOS     IN VARCHAR2,
                             PV_TIPO_DEFENSA_ESP       IN VARCHAR2,
                             PN_KMS_MOMENTO_ASEG       IN NUMBER,
                             PV_ACRED_PREND            IN VARCHAR2,
                             P_CERO_KM                 IN VARCHAR2,
                             P_MENSAJE                 IN OUT VARCHAR2) IS
    MENSAJE1 VARCHAR2(100);
    MENSAJE2 VARCHAR2(100);
    MENSAJE3 VARCHAR2(100);
    MENSAJE4 VARCHAR2(100);
    MENSAJE5 VARCHAR2(100);
    MENSAJE6 VARCHAR2(100);
    MENSAJE7 VARCHAR2(100);
    EXISTE   VARCHAR2(1);
  BEGIN
    /*- 1- VERIF_PROPIEDAD_VEH -*/
    IF PN_VERIF_PROPIEDAD_VEH IS NOT NULL THEN
      BEGIN
        SELECT 'S'
          INTO EXISTE
          FROM G7000026
         WHERE COD_CIA = 1
           AND COD_CAMPO = 'COMP_TITUL_VEH'
           AND COD_LISTA = PN_VERIF_PROPIEDAD_VEH
           AND ROWNUM = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          MENSAJE1 := 'El valor de VERIF_PROPIEDAD_VEH no es valido';
        WHEN OTHERS THEN
          NULL;
      END;
    ELSE
      MENSAJE1 := 'El valor de VERIF_PROPIEDAD_VEH es nulo';
    END IF;
  
    /*- 2- ESTADO_GENERAL -*/
    IF PN_ESTADO_GENERAL IS NOT NULL THEN
      IF PN_ESTADO_GENERAL NOT IN (0, 1, 2, 3, 4, 5, 9) THEN
        MENSAJE2 := 'El valor de ESTADO_GENERAL no es valido';
      END IF;
    ELSE
      MENSAJE2 := 'El valor de ESTADO_GENERAL es nulo';
    END IF;
  
    /*- 3- TIPO_VIDRIO_TONALIZADO -*/
    IF PN_TIPO_VIDRIO_TONALIZADO IS NOT NULL THEN
      IF PN_TIPO_VIDRIO_TONALIZADO NOT IN (1, 2, 3) THEN
        MENSAJE3 := 'El valor de TIPO_VIDRIO_TONALIZADO no es valido';
      END IF;
    ELSE
      MENSAJE3 := 'El valor de TIPO_VIDRIO_TONALIZADO es nulo';
    END IF;
  
    /*- 4- CRISTALES_GRABADOS -*/
    IF PV_CRISTALES_GRABADOS IS NOT NULL THEN
      IF PV_CRISTALES_GRABADOS NOT IN ('S', 'N') THEN
        MENSAJE4 := 'El valor de CRISTALES_GRABADOS no es valido';
      END IF;
    ELSE
      MENSAJE4 := 'El valor de CRISTALES_GRABADOS es nulo';
    END IF;
  
    /*- 5- TIPO_DEFENSA_ESP -*/
    IF PV_TIPO_DEFENSA_ESP IS NOT NULL THEN
      IF PV_TIPO_DEFENSA_ESP NOT IN ('1', '2', '3') THEN
        MENSAJE5 := 'El valor de TIPO_DEFENSA_ESP no es valido';
      END IF;
    ELSE
      MENSAJE5 := 'El valor de TIPO_DEFENSA_ESP es nulo';
    END IF;
  
    /*- 6- KMS_MOMENTO_ASEG -*/
    IF PN_KMS_MOMENTO_ASEG IS NOT NULL THEN
      IF PN_KMS_MOMENTO_ASEG < 0 THEN
        MENSAJE6 := 'El Kilometraje debe ser mayor a 0';
      ELSIF PN_KMS_MOMENTO_ASEG = 0 THEN
        IF P_CERO_KM != 'SI' THEN
          MENSAJE6 := 'El Kilometraje no puede ser 0 si no es 0km';
        END IF;
      ELSE
        IF PN_KMS_MOMENTO_ASEG != FLOOR(PN_KMS_MOMENTO_ASEG) THEN
          MENSAJE6 := 'El Kilometraje debe ser un Valor Entero sin puntos o comas';
        END IF;
      END IF;
    ELSE
      IF P_CERO_KM != 'SI' THEN
        MENSAJE6 := 'El Kilometraje no puede ser nulo si no es 0km';
      END IF;
    END IF;
  
    /*- 7- ACRED_PREND -*/
    -- no se valida nada
  
    /*- CONCATENO LOS MENSAJES DE ERROR -*/
    IF MENSAJE1 IS NOT NULL THEN
      IF P_MENSAJE IS NULL THEN
        P_MENSAJE := MENSAJE1;
      ELSE
        P_MENSAJE := P_MENSAJE || '/' || MENSAJE1;
      END IF;
    END IF;
  
    IF MENSAJE2 IS NOT NULL THEN
      IF P_MENSAJE IS NULL THEN
        P_MENSAJE := MENSAJE2;
      ELSE
        P_MENSAJE := P_MENSAJE || '/' || MENSAJE2;
      END IF;
    END IF;
  
    IF MENSAJE3 IS NOT NULL THEN
      IF P_MENSAJE IS NULL THEN
        P_MENSAJE := MENSAJE3;
      ELSE
        P_MENSAJE := P_MENSAJE || '/' || MENSAJE3;
      END IF;
    END IF;
  
    IF MENSAJE4 IS NOT NULL THEN
      IF P_MENSAJE IS NULL THEN
        P_MENSAJE := MENSAJE4;
      ELSE
        P_MENSAJE := P_MENSAJE || '/' || MENSAJE4;
      END IF;
    END IF;
  
    IF MENSAJE5 IS NOT NULL THEN
      IF P_MENSAJE IS NULL THEN
        P_MENSAJE := MENSAJE5;
      ELSE
        P_MENSAJE := P_MENSAJE || '/' || MENSAJE5;
      END IF;
    END IF;
  
    IF MENSAJE6 IS NOT NULL THEN
      IF P_MENSAJE IS NULL THEN
        P_MENSAJE := MENSAJE6;
      ELSE
        P_MENSAJE := P_MENSAJE || '/' || MENSAJE6;
      END IF;
    END IF;
  
    IF MENSAJE7 IS NOT NULL THEN
      IF P_MENSAJE IS NULL THEN
        P_MENSAJE := MENSAJE7;
      ELSE
        P_MENSAJE := P_MENSAJE || '/' || MENSAJE7;
      END IF;
    END IF;
  END VALIDACION_DATOS;

  /**
  @Nombre          CONTROLES_TECNICOS
  @Descripcion     Ejecuta los controles tecnicos a nivel de poliza y riesgo
  @Aplicacion      POLARIS_WINDOWS
  @Modulo          Modulo
  @Funcion         Funcion
  @Tipo            Logica Negocios
  @Retorno         --
  @Autor           MARTINEZR
  @Fecha_Creacion  23/11/2005
  @Palabras_Claves controles tecnicos
  @Reusabilidad    Baja
  @Activo          S
  
  @Param_Nombre    p_nro_presup
  @Param_Descrip   numero de presupuesto
  @Param_Nombre    p_cod_cia
  @Param_Descrip   corresponde al codigo de cia
  @Param_Nombre    p_cod_secc
  @Param_Descrip   corresponde al codigo de seccion
  @Param_Nombre    p_cod_ramo
  @Param_Descrip   corresponde al codigo de ramo
  @Param_Nombre    p_agencia
  @Param_Descrip   corresponde al codigo de agencia
  @Param_Nombre    p_error
  @Param_Descrip   parametro de salida
  
  */
  PROCEDURE CONTROLES_TECNICOS(P_NRO_PRESUP IN NUMBER,
                               P_COD_CIA    IN NUMBER,
                               P_COD_SECC   IN NUMBER,
                               P_COD_RAMO   IN NUMBER,
                               P_AGENCIA    IN NUMBER,
                               P_ERROR      IN OUT VARCHAR2) IS
    P_NIVEL VARCHAR2(8);
    ERROR_CONTROL EXCEPTION;
    CTRL_TECNICO EXCEPTION;
  
    CURSOR CUR_ERRORES IS
      SELECT DISTINCT B.COD_ERROR || ' - ' || B.DESC_ERROR DESC_ERROR
        FROM X2000220 A, G2000210 B
       WHERE NUM_SECU_POL = P_NRO_PRESUP
         AND A.COD_RECHAZO IN (2, 3)
         AND A.NIVEL_AUT =
             (SELECT MIN(NIVEL_AUT)
                FROM X2000220
               WHERE NUM_SECU_POL = A.NUM_SECU_POL
                 AND DSNIVEL = A.DSNIVEL
                 AND COD_RECHAZO = A.COD_RECHAZO)
         AND A.COD_ERROR = B.COD_ERROR;
  BEGIN
    P_ERROR := NULL;
  
    P_SET_VARIABLES(NULL, NULL, NULL, NULL, NULL, NULL, NULL); -- cyudowsky req 182457 06/05/2011
  
    BEGIN
      -- Ejecuta controles tecnicos nivel poliza.
      P_NIVEL := 'POLIZA';
      P_CTRL_F45_NIV_POLIZA(P_NRO_PRESUP,
                            0, --numero de endoso
                            P_COD_CIA, --compania
                            P_COD_SECC, --seccion
                            NVL(P_COD_RAMO, 1), --ramo
                            NULL, --cod_ries
                            1, --dsnivel
                            2, --cod_sistema
                            NULL, --tipo endoso
                            P_AGENCIA); --agencia control tecnico
      -- Ejecuta controles tecnicos nivel riesgo
      P_NIVEL := 'RIESGO';
      P_CTRL_F45_NIV_COBERTURA(P_NUMSECUPOL => P_NRO_PRESUP,
                               P_NUMEND     => 0,
                               P_CODCIA     => P_COD_CIA,
                               P_CODSECC    => P_COD_SECC,
                               P_CODRAMO    => NVL(P_COD_RAMO, 1),
                               P_CODRIES    => 1,
                               P_DSNIVEL    => 'DC',
                               P_CODSIST    => 2,
                               P_TIPOEND    => NULL,
                               P_AGENCIA_CT => P_AGENCIA);
      --p_ctrl_f45_niv_cobertura
      --dsnivel=> 'DC'
      /*      p_ctrl_f45_niv_poliza( p_nro_presup,
      0,       --numero de endoso
      p_cod_cia,   --compania
      p_cod_secc,  --seccion
      NVL(p_cod_ramo,1),  --ramo
      1,      --cod_ries
      2,   --dsnivel
      2,   --cod_sistema
      NULL,   --tipo endoso
      p_agencia);  --agencia control tecnico*/
    EXCEPTION
      WHEN OTHERS THEN
        P_ERROR := '1-Error al ejecutar controles tecnicos a nivel: ' ||
                   P_NIVEL || ' - ' || SQLERRM;
        RAISE ERROR_CONTROL;
    END;
  
    /* Si existe algun control tecnico rechazado (2) , entonces grabar error y
        establecer en la tabla emi_prod_autos el campo cod_estado = 'RE' para reflejar
    el hecho de este error */
    FOR ERRORES IN CUR_ERRORES LOOP
      P_ERROR := P_ERROR || ERRORES.DESC_ERROR;
    END LOOP;
  
    IF LENGTH(P_ERROR) > 0 THEN
      --actualizar emi_prod_autos
      BEGIN
        P_ERROR := '2-Rechazo por control tecnico rechazado o a autorizar';
        RAISE CTRL_TECNICO;
      EXCEPTION
        WHEN CTRL_TECNICO THEN
          RAISE ERROR_CONTROL;
        WHEN OTHERS THEN
          P_ERROR := '3-Error al actualizar el estado en la tabla emi_prod_autos - ' ||
                     SQLERRM;
          RAISE ERROR_CONTROL;
      END;
    END IF;
  EXCEPTION
    WHEN ERROR_CONTROL THEN
      NULL;
    WHEN OTHERS THEN
      P_ERROR := '4-Error al tratar controles tecnicos - ' || SQLERRM;
  END CONTROLES_TECNICOS;

  /**
  @Nombre          EJECUTA_COBOL
  @Descripcion     Ejecuta el facturador (AB100277)
  @Aplicacion      POLARIS_WINDOWS
  @Modulo          Modulo
  @Funcion         Funcion
  @Tipo            Logica Negocios
  @Retorno         --
  @Autor           MARTINEZR
  @Fecha_Creacion  23/11/2005
  @Palabras_Claves ejecuta cobol facturador
  @Reusabilidad    Baja
  @Activo          S
  
  @Param_Nombre    p_cia
  @Param_Descrip   corresponde al codigo de cia
  @Param_Nombre    p_secc
  @Param_Descrip   corresponde al codigo de seccion
  @Param_Nombre    p_ramo
  @Param_Descrip   corresponde al codigo de ramo
  @Param_Nombre    p_num_pol
  @Param_Descrip   corresponde al numero de poliza a facturar
  @Param_Nombre    p_num_end
  @Param_Descrip   corresponde al numero de endoso
  @Param_Nombre    p_mensaje
  @Param_Descrip   mensaje de error de salida
  
  */
  PROCEDURE EJECUTA_COBOL(P_CIA     IN NUMBER,
                          P_SECC    IN NUMBER,
                          P_RAMO    IN NUMBER,
                          P_NUM_POL IN NUMBER,
                          P_NUM_END IN NUMBER,
                          P_MENSAJE IN OUT VARCHAR2) IS
    FORM_A       VARCHAR2(10);
    A_SETEO      VARCHAR2(8);
    A_TIPO_SETEO VARCHAR2(1);
    A_TERMI      VARCHAR2(20);
    A_SESION     VARCHAR2(8);
    A_FECHA      VARCHAR2(10);
    A_NOMPROG    VARCHAR2(50);
    A_NOMBRE     VARCHAR2(20);
    PROG         VARCHAR2(20);
    LISTA        VARCHAR2(20);
    USU1         VARCHAR2(10);
    W_CLAVE      VARCHAR2(5);
    W_NOMBLIS    VARCHAR2(15);
    PL_AUX       NUMBER;
  BEGIN
    PROG  := 'AB100277.INP';
    LISTA := 'AB100277.PCO';
  
    BEGIN
      PCK_PRE0080.GENERA_A2990050(REPLACE(USER, 'OPS$', ''),
                                  FORM_A,
                                  A_SETEO,
                                  A_TIPO_SETEO,
                                  A_TERMI,
                                  A_SESION,
                                  A_FECHA,
                                  A_NOMPROG,
                                  A_NOMBRE,
                                  PROG,
                                  LISTA);
    EXCEPTION
      WHEN OTHERS THEN
        P_MENSAJE := 'Error al generar A2990050 - ' || SQLERRM;
        RETURN;
    END;
  
    USU1 := SUBSTR(UPPER(REPLACE(USER, 'OPS$', '')), 1, 3);
  
    BEGIN
      --w_clave := TO_CHAR (SYSDATE, 'SSSSS');
      SELECT REPLACE(TO_CHAR(OPS$TRONADOR.A2990050_SEQ.NEXTVAL, '09999'),
                     ' ')
        INTO W_CLAVE
        FROM DUAL;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,
                                'Error al rescatar la clave: ' || SQLERRM);
    END;
  
    W_NOMBLIS := USU1 || FORM_A || SUBSTR(TO_CHAR(SYSDATE, 'SSSSS'), 3, 3) ||
                 '.lis';
  
    BEGIN
      INSERT INTO A2990050
        (CLAVE,
         USUARIO,
         TERMINAL,
         SESION,
         FECHA,
         MCALIS,
         PARA1,
         PARA2,
         PARA3,
         PARA4,
         PARA5,
         PARAFECHA1,
         PARAFECHA2,
         PARAFECHA3,
         NOMBRPT,
         NOMBLIS,
         COPIAS,
         HOJAS,
         OBSERVACION,
         IMPRESORA,
         MANTIENE,
         PARA6,
         PARA7,
         NOMBARC,
         MCA_CIERRE,
         FECHA_CIERRE,
         COD_AGENCIA,
         COD_JOB,
         ENTRADA_ID,
         NIVEL,
         NOM_USER,
         SETEO,
         TIPO_SETEO)
      VALUES
        (W_CLAVE,
         REPLACE(USER, 'OPS$', ''),
         A_TERMI,
         A_SESION,
         TRUNC(SYSDATE),
         'S',
         P_CIA,
         P_SECC,
         P_NUM_POL,
         P_NUM_END,
         NULL,
         NULL,
         NULL,
         NULL,
         'AB100277.pco',
         W_NOMBLIS,
         1,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         TO_CHAR(SYSDATE, 'SSSSS'),
         NULL,
         A_NOMBRE,
         A_SETEO,
         A_TIPO_SETEO);
    EXCEPTION
      WHEN OTHERS THEN
        P_MENSAJE := 'Error al insertar en A2990050 - ' || SQLERRM;
        RETURN;
    END;
  
    COMMIT;
  
    BEGIN
      PL_AUX := DAEMON.EJECUTAR('RUN_COBOL',
                                FALSE,
                                'AB100277',
                                'PARACOB1=' || P_NUM_POL);
    EXCEPTION
      WHEN OTHERS THEN
        P_MENSAJE := 'Error al ejecutar el deamon - ' || SQLERRM;
    END;
  END EJECUTA_COBOL;

  /**
  @Nombre          VALIDA_PROVISORIA
  @Descripcion     Valida si la poliza es provisoria
  @Aplicacion      POLARIS_WINDOWS
  @Modulo          Modulo
  @Funcion         Funcion
  @Tipo            Logica Negocios
  @Retorno         BOOLEAN
  @Autor           MARTINEZR
  @Fecha_Creacion  23/11/2005
  @Palabras_Claves valida poliza provisoria
  @Reusabilidad    Baja
  @Activo          S
  
  @Param_Nombre    p_cia
  @Param_Descrip   corresponde al codigo de cia
  @Param_Nombre    p_ramo
  @Param_Descrip   corresponde al codigo de ramo
  @Param_Nombre    p_num_pol1
  @Param_Descrip   corresponde al numero de poliza a facturar
  
  */
  FUNCTION VALIDA_PROVISORIA(P_CIA      IN NUMBER,
                             P_RAMO     IN NUMBER,
                             P_NUM_POL1 IN NUMBER) RETURN BOOLEAN IS
    AUX VARCHAR2(1) := NULL;
  BEGIN
    BEGIN
      SELECT NVL(MCA_PROVISORIO, 'N')
        INTO AUX
        FROM A2000030
       WHERE COD_CIA = P_CIA
         AND COD_RAMO = P_RAMO
         AND NUM_POL1 = P_NUM_POL1
         AND NUM_END = 0
         AND ROWNUM = 1;
    
      IF AUX != 'S' THEN
        RETURN TRUE; -- La poliza NO es provisoria
      ELSE
        RETURN FALSE; -- La poliza SI es provisoria
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20008,
                                'Error al validar poliza provisoria - ' ||
                                SQLERRM);
    END;
  END VALIDA_PROVISORIA;

  /**
  @Nombre          RECUPERA_PARAMETROS
  @Descripcion     Recupera parametros necesarios
  @Aplicacion      POLARIS_WINDOWS
  @Modulo          Modulo
  @Funcion         Funcion
  @Tipo            Logica Negocios
  @Retorno         --
  @Autor           MARTINEZR
  @Fecha_Creacion  23/11/2005
  @Palabras_Claves recupera parametros
  @Reusabilidad    Baja
  @Activo          S
  
  @Param_Nombre    p_codcia
  @Param_Descrip   corresponde al codigo de cia
  @Param_Nombre    p_codsecc
  @Param_Descrip   corresponde al codigo de seccion
  @Param_Nombre    p_codramo
  @Param_Descrip   corresponde al codigo de ramo
  @Param_Nombre    p_nro_presup
  @Param_Descrip   corresponde al numero de presupuesto
  @Param_Nombre    p_cod_prod
  @Param_Descrip   salida - codigo de productor
  @Param_Nombre    p_id
  @Param_Descrip   salida - id
  @Param_Nombre    p_id_riesgo
  @Param_Descrip   salida - id domicilio ubicacion riesgo
  @Param_Nombre    p_telefono
  @Param_Descrip   salida - numero de telefono concatenado
  @Param_Nombre    pv_mensaje_error
  @Param_Descrip   Mensaje de error de salida
  
  */
  PROCEDURE RECUPERA_PARAMETROS(P_CODCIA         IN NUMBER,
                                P_CODSECC        IN NUMBER,
                                P_CODRAMO        IN NUMBER,
                                P_NRO_PRESUP     IN NUMBER,
                                P_COD_PROD       OUT NUMBER,
                                P_ID             OUT NUMBER,
                                P_ID_RIESGO      OUT NUMBER,
                                P_TELEFONO       OUT VARCHAR2,
                                P_FECHAVIG       OUT DATE,
                                PV_MENSAJE_ERROR IN OUT VARCHAR2) IS
  BEGIN
    BEGIN
      SELECT COD_PROD, ID, ID_DOMIC_UBI_RIESGO, FECHA_VIG
        INTO P_COD_PROD, P_ID, P_ID_RIESGO, P_FECHAVIG
        FROM EMI_PRESU_SOLIC
       WHERE COD_CIA = P_CODCIA
         AND COD_SECC = P_CODSECC
         AND COD_RAMO = P_CODRAMO
         AND NRO_PRESUP = P_NRO_PRESUP
         AND ROWNUM = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        PV_MENSAJE_ERROR := 'No se encontraron datos en EMI_PRESU_SOLIC';
        RETURN;
      WHEN OTHERS THEN
        PV_MENSAJE_ERROR := 'Error al recuperar parametros de EMI_PRESU_SOLIC';
        RETURN;
    END;
    --   BEGIN
    --     SELECT caracteristica || numero_telefono
    --  INTO   p_telefono
    --  FROM   tb_telefonos
    --  WHERE  cif_id = p_id
    --  AND    act_f = (SELECT MAX(act_f)
    --              FROM tb_telefonos
    --          WHERE cif_id = p_id);
    --   EXCEPTION
    --     WHEN NO_DATA_FOUND THEN
    --    pv_mensaje_error := 'No se encontraron datos en TB_TELEFONOS';
    --  WHEN OTHERS THEN
    --    pv_mensaje_error := 'Error al recuperar parametros de TB_TELEFONOS';
    --   END;
  END RECUPERA_PARAMETROS;

  /**
  @Nombre          TIENE_CT_A_AUTORIZAR
  @Descripcion     Determina si tiene CT a autorizar
  @Aplicacion      POLARIS_WINDOWS
  @Modulo          Modulo
  @Funcion         Funcion
  @Tipo            Logica Negocios
  @Retorno         --
  @Autor           MARTINEZR
  @Fecha_Creacion  29/11/2005
  @Palabras_Claves determina controles tecnicos a autorizar
  @Reusabilidad    Baja
  @Activo          S
  
  @Param_Nombre    p_nro_presup
  @Param_Descrip   corresponde al numero de presupuesto
  
  */
  FUNCTION TIENE_CT_A_AUTORIZAR(P_NRO_PRESUP IN NUMBER) RETURN BOOLEAN IS
    AUX NUMBER := 0;
  BEGIN
    BEGIN
      SELECT 1
        INTO AUX
        FROM X2000220
       WHERE COD_RECHAZO = 3
         AND NUM_SECU_POL = P_NRO_PRESUP
         AND ROWNUM = 1;
    
      RETURN TRUE;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RETURN FALSE;
      WHEN OTHERS THEN
        NULL;
    END;
  END TIENE_CT_A_AUTORIZAR;

  --31/03/2006 - MONICAF - Verifica si una poliza es o no del banco rio
  FUNCTION F_ES_POL_BCO_RIO(PN_COD_CIA  IN NUMBER,
                            PN_COD_SECC IN NUMBER,
                            PN_COD_RAMO IN NUMBER,
                            PN_NUM_POL1 IN NUMBER) RETURN BOOLEAN IS
    CURSOR C_A2000030(P_NUM_POL1 IN NUMBER) IS
      SELECT *
        FROM A2000030
       WHERE COD_CIA = PN_COD_CIA
         AND COD_SECC = PN_COD_SECC
         AND COD_RAMO = PN_COD_RAMO
         AND COD_INICIADOR = 72038
         AND NUM_POL1 = P_NUM_POL1;
  
    PR_A2000030 C_A2000030%ROWTYPE;
  
    CURSOR C_A2040100(P_NUM_SECU_POL IN NUMBER) IS
      SELECT COD_RAMO_VEH, DECODE(MCA_0KM, 'S', 'SI', 'N', 'NO', NULL)
        FROM A2040100
       WHERE NUM_SECU_POL = P_NUM_SECU_POL
         AND NUM_END = (SELECT MAX(NUM_END)
                          FROM A2040100
                         WHERE NUM_SECU_POL = P_NUM_SECU_POL);
  
    LN_COD_RAMO_VEH A2040100.COD_RAMO_VEH%TYPE := NULL;
    LV_MCA_0KM      VARCHAR2(2) := NULL;
  BEGIN
    OPEN C_A2000030(PN_NUM_POL1);
  
    FETCH C_A2000030
      INTO PR_A2000030;
  
    IF C_A2000030%FOUND THEN
      OPEN C_A2040100(PR_A2000030.NUM_SECU_POL);
    
      FETCH C_A2040100
        INTO LN_COD_RAMO_VEH, LV_MCA_0KM;
    
      IF C_A2040100%FOUND THEN
        CLOSE C_A2040100;
      
        CLOSE C_A2000030;
      
        IF PCK_AUTOS_PLANES.F_DEV_PLAN_RECUPERADOR(LN_COD_RAMO_VEH,
                                                   NVL(LV_MCA_0KM, 'NO')) THEN
          RETURN TRUE;
        ELSE
          RETURN FALSE;
        END IF;
      END IF;
    
      CLOSE C_A2040100;
    END IF;
  
    CLOSE C_A2000030;
  
    RETURN FALSE;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,
                              'Error en FUNCION PCK_AUTOS_EMISION.es_pol_bco_rio ' ||
                              SQLERRM);
  END F_ES_POL_BCO_RIO;
  
  PROCEDURE P_RECUPERA_A1040400 ( PN_COD_CIA    IN A1040400.COD_CIA%TYPE   ,
                                PV_COD_MARCA  IN A1040400.COD_MARCA%TYPE ,
                                PD_FECHA_VIG  IN DATE  := TRUNC(SYSDATE) ,
                                REG_A1040400  OUT r_a1040400  ) as
                                
                                
                                
        begin return;
        end P_RECUPERA_A1040400;

  ----------------------------------------------------------------------------  --

  /**
  @Nombre          P_RECUPERA_DATOS_EQUIPO_RECU
  @Descripcion     Recupera Datos del equipo recuperador
  @Aplicacion      POLARIS_WINDOWS
  @Modulo          Modulo
  @Funcion         Funcion
  @Tipo            Logica Negocios
  @Retorno         --
  @Autor           Matias de Toro
  @Fecha_Creacion  05/05/2006
  @Palabras_Claves equipo recu
  @Reusabilidad    Baja
  @Activo          S
  
  @Param_Nombre    p_codcia
  @Param_Descrip   corresponde al codigo de cia
  @Param_Nombre    p_cod_secc
  @Param_Descrip   corresponde al codigo de seccion
  @Param_Nombre    p_cod_ramo
  @Param_Descrip   corresponde al codigo de ramo
  @Param_Nombre    p_nro_presup
  @Param_Descrip   corresponde al numero de presupuesto
  @Param_Nombre    p_cod_prod
  @Param_Descrip   codigo de productor
  @Param_Nombre    p_fecha_vig
  @Param_Descrip   corresponde la fecha de vigencia
  @Param_Nombre    p_error
  @Param_Descrip   salida - errores detectados
  @Param_Nombre    p_modelo_equipo
  @Param_Descrip   salida - modelo del equipo recuperador
  @Param_Nombre    p_proveedor_equipo
  @Param_Descrip   salida - proveedor del equipo recuperador
  
  */
  PROCEDURE P_RECUPERA_DATOS_EQUIPO_RECU(P_COD_CIA          IN NUMBER,
                                         P_COD_SECC         IN NUMBER,
                                         P_COD_RAMO         IN NUMBER,
                                         P_NRO_PRESUP       IN NUMBER,
                                         P_COD_PROD         IN NUMBER,
                                         P_FECHA_VIG        IN DATE,
                                         P_ERROR            OUT VARCHAR2,
                                         P_MODELO_EQUIPO    OUT VARCHAR2,
                                         P_PROVEEDOR_EQUIPO OUT NUMBER) IS
    PB_OBLIGATORIO_EQUIPO  BOOLEAN;
    P_CUMPLE_LO_JACK       BOOLEAN;
    P_OBLIGATORIO_INTERIOR BOOLEAN;
  BEGIN
    PCK_AUTOS_SOLIC.P_RECU_LO_JACK(P_COD_CIA,
                                   P_COD_SECC,
                                   P_COD_RAMO,
                                   P_NRO_PRESUP,
                                   P_COD_PROD,
                                   P_FECHA_VIG,
                                   P_ERROR,
                                   PB_OBLIGATORIO_EQUIPO,
                                   P_MODELO_EQUIPO,
                                   P_PROVEEDOR_EQUIPO,
                                   P_CUMPLE_LO_JACK,
                                   P_OBLIGATORIO_INTERIOR);
  END P_RECUPERA_DATOS_EQUIPO_RECU;

/*****************************************************************************************************/
PROCEDURE COTIZAR_POLIZA(lote IN CLOB,
                           resultado OUT CLOB
                         ) as
                         

  NNRO_PRESUP NUMBER;
  VACCESORIOS VARCHAR2(200);
  NVALOR_VEH NUMBER;
  VPROVINCIA VARCHAR2(200);
  NPROVINCIA NUMBER;
  VTIPO_VEHICULO VARCHAR2(200);
  NTIPO_VEHICULO NUMBER;
  VMODELO VARCHAR2(200);
  VCOD_MARCA_OUT VARCHAR2(200);
  NCOD_CLASE NUMBER;
  VMARCA_IMPORTADO VARCHAR2(200);
  RPLANES CONSTANTES.REF_CURSOR;
  RACCESORIOS CONSTANTES.REF_CURSOR;
  VMENSAJE_ERROR VARCHAR2(200);
  accesorios_count number;
  CODIGO VARCHAR2(200);
  TIPO VARCHAR2(200);
  VALOR VARCHAR2(200);
  ACCESORIOS_OBJ REG_ACCESORIOS;
  ACCESORIOS_ARRAY VA_ACCESORIOS;
  ERROR_COTIZADOR EXCEPTION;
  begin
  apex_json.parse(lote);
  ACCESORIOS_ARRAY:=VA_ACCESORIOS();
  -- Meter accesorios en VA_ACCESORIOS
  accesorios_count    := apex_json.get_count(p_path => 'accesorios');
  FOR i IN 1..accesorios_count
        LOOP
          BEGIN
            ACCESORIOS_ARRAY.EXTEND(1);
            CODIGO:=apex_json.get_varchar2(p_path=>'accesorios[%d].codigo',p0=>i);
            TIPO:=apex_json.get_varchar2(p_path=>'accesorios[%d].tipo',p0=>i);
            VALOR:=apex_json.get_varchar2(p_path=>'accesorios[%d].valor',p0=>i);
            ACCESORIOS_OBJ:= REG_ACCESORIOS(CODIGO,TIPO,VALOR);
            ACCESORIOS_ARRAY(i):=ACCESORIOS_OBJ;
          EXCEPTION
              WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('Error recorriendo accesorios  '||sqlerrm);
                NULL;
              END;
        END LOOP;
--Invocar PCK_WEBSERVICE_COTIZACION.COTIZADOR     
    /*PCK_WEBSERVICE_COTIZACION.COTIZADOR(
    NPRODUCTOR => apex_json.get_number(p_path => 'productor'),
    VTIPO_DOCUMENTO => apex_json.get_varchar2(p_path => 'tipoDocumento'),
    NNUMERO_DOCUMENTO => apex_json.get_number(p_path => 'numeroDocumento'),
    VAPELLIDO => apex_json.get_varchar2(p_path => 'apellido'),
    VNOMBRES => apex_json.get_varchar2(p_path => 'nombres'),
    VCODIGO_POSTAL => apex_json.get_varchar2(p_path => 'codigoPostal'),
    NCONDICION_IVA => apex_json.get_number(p_path => 'iva'),
    VPREFIJO => apex_json.get_varchar2(p_path => 'prefijo'),
    VTELEFONO => apex_json.get_varchar2(p_path => 'telefono'),
    NPORCENTAJE => apex_json.get_number(p_path => 'porcentajeAjuste'),
    VNEGOCIO => apex_json.get_varchar2(p_path => 'negocio'),
    NANIO => apex_json.get_number(p_path => 'anio'),
    VPATENTE => apex_json.get_varchar2(p_path => 'patente'),
    VCERO_KM => apex_json.get_varchar2(p_path => 'ceroKM'),
    NCODIGO_INFOAUTO => apex_json.get_varchar2(p_path => 'codInfoAuto'),
    VCOD_MARCA => apex_json.get_varchar2(p_path => 'codigoMarca'),
    VGNC => apex_json.get_varchar2(p_path => 'gnc'),
    VALORGNC => apex_json.get_varchar2(p_path => 'valorGnc'),
    DFECHA_VIGENCIA => apex_json.get_date(p_path => 'fechaVigencia'),
    NGRUPO_AFINIDAD => apex_json.get_number(p_path => 'grupoAfinidad'),
    VUSO_COMERCIAL => apex_json.get_varchar2(p_path => 'uso'), 
    VROBO_CONTENIDO => apex_json.get_varchar2(p_path => 'roboContenido'),
    P_REG_ACC => ACCESORIOS_ARRAY,      -----------------
    VFECHA_NAC => apex_json.get_varchar2(p_path => 'fechaNac'),
    VEMAIL => apex_json.get_varchar2(p_path => 'fechaNac'),
    VPREFIJO2 => apex_json.get_varchar2(p_path => 'prefijo2'),
    VTELEFONO2 => apex_json.get_varchar2(p_path => 'telefono2'),
    VPREFIJO3 => apex_json.get_varchar2(p_path => 'prefijo3'),
    VTELEFONO3 => apex_json.get_varchar2(p_path => 'telefono3'),
    VPREFIJO4 => apex_json.get_varchar2(p_path => 'prefijo4'),
    VTELEFONO4 => apex_json.get_varchar2(p_path => 'telefono4'),
    VCHASIS => apex_json.get_varchar2(p_path => 'chasis'),
    VMOTOR => apex_json.get_varchar2(p_path => 'motor'),
    VIDBOL => apex_json.get_varchar2(p_path => 'idBol'),
    VCONSNOMBRE => apex_json.get_varchar2(p_path => 'nombreCons'),
    VCONSTEL => apex_json.get_varchar2(p_path => 'telCons'),
    VSUCU => apex_json.get_varchar2(p_path => 'sucursal'),
    VADMINIS => apex_json.get_varchar2(p_path => 'administrador'),
    VVENDEDOR => apex_json.get_varchar2(p_path => 'vendedor'),
    NVALOR_VEH_IN => apex_json.get_number(p_path => 'valorVeh '),
    NNRO_PRESUP => NNRO_PRESUP,
    VACCESORIOS => VACCESORIOS,
    NVALOR_VEH => NVALOR_VEH,
    VPROVINCIA => VPROVINCIA,
    NPROVINCIA => NPROVINCIA,
    VTIPO_VEHICULO => VTIPO_VEHICULO,
    NTIPO_VEHICULO => NTIPO_VEHICULO,
    VMODELO => VMODELO,
    VCOD_MARCA_OUT => VCOD_MARCA_OUT,
    NCOD_CLASE => NCOD_CLASE,
    VMARCA_IMPORTADO => VMARCA_IMPORTADO,
    RPLANES => RPLANES,
    RACCESORIOS => RACCESORIOS,
    VMENSAJE_ERROR => VMENSAJE_ERROR
  );
  
  IF (VMENSAJE_ERROR != null)--chequear que retorna cuando se finalice SP
    THEN
        raise error_cotizador;
    END IF;
    
--Invocar PCK_EARNIX_ALTA.DEVOLVER_COTIZACION
  PCK_AUTOS_EMISION.DEVOLVER_COTIZACION(
    NNRO_PRESUP => NNRO_PRESUP,
    VACCESORIOS => VACCESORIOS,
    NVALOR_VEH => NVALOR_VEH,
    VPROVINCIA => VPROVINCIA,
    NPROVINCIA => NPROVINCIA,
    VTIPO_VEHICULO => VTIPO_VEHICULO,
    NTIPO_VEHICULO => NTIPO_VEHICULO,
    VMODELO => VMODELO,
    VCOD_MARCA_OUT => VCOD_MARCA_OUT,
    NCOD_CLASE => NCOD_CLASE,
    VMARCA_IMPORTADO => VMARCA_IMPORTADO,
    RPLANES => RPLANES,
    RACCESORIOS => RACCESORIOS,
    RESULTADO => RESULTADO,
    VMENSAJE_ERROR => VMENSAJE_ERROR
  );
  */
  
    apex_json.initialize_clob_output;
    apex_json.open_object;                                                  --{
    apex_json.write('codigo',codigo);
    apex_json.write('tipo',tipo);
    apex_json.write('valor',valor);
    apex_json.close_object;
    resultado := apex_json.get_clob_output;
    apex_json.free_output;
    
  EXCEPTION
  WHEN error_cotizador then 
        get_exception('error_cotizador',resultado);
  WHEN OTHERS THEN
        get_exception('general',resultado);
  end COTIZAR_POLIZA;

--------------------------------------------------------------------------------
  PROCEDURE DEVOLVER_COTIZACION(  nnro_presup       IN NUMBER,
                                  vaccesorios       IN VARCHAR2,
                                  nvalor_veh        IN NUMBER,
                                  vprovincia        IN VARCHAR2,
                                  nprovincia        IN NUMBER,
                                  vtipo_vehiculo    IN VARCHAR2,
                                  ntipo_vehiculo    IN NUMBER,
                                  vmodelo           IN VARCHAR2,
                                  vcod_marca_out    IN VARCHAR2,
                                  ncod_clase        IN NUMBER,
                                  vmarca_importado  IN VARCHAR2,
                                  rplanes           IN CONSTANTES.REF_CURSOR,
                                  raccesorios       IN CONSTANTES.REF_CURSOR,
                                  resultado         OUT CLOB,
                                  vmensaje_error    OUT VARCHAR2) as
                                  
    NRO_ORDEN NUMBER;               --de donde lo saco?
    DESCRIPCION_AUTO VARCHAR2(50);        --de donde lo saco?
    PLANES_CODIGO VARCHAR2(50);           --EARNIX_PLANES.COD_PLAN
    PLANES_DEBITO VARCHAR2(50);           --EARNIX_PLANES.
    PLANES_DESCRIPCION VARCHAR2(50);      --EARNIX_PLANES.
    PLANES_EFECTIVO VARCHAR2(50);         --EARNIX_PLANES.
    PLANES_FRANQUICIA VARCHAR2(50);       --EARNIX_PLANES.
    PLANES_FRANQUICIA_ROBO VARCHAR2(50);  --EARNIX_PLANES.
    PLANES_INSPECCIONABLE VARCHAR2(50);   --EARNIX_PLANES.
    PLANES_PRIMA VARCHAR2(50);            --EARNIX_PLANES.
    PLANES_PRIMA_EFECTIVO VARCHAR2(50);   --EARNIX_PLANES.
    PLANES_CON_RECUPERADOR VARCHAR2(50);  --EARNIX_PLANES.
    COBERT_CODIGO VARCHAR2(50);
    COBERT_IMPORTE VARCHAR2(50);
    IMPUESTO_CODIGO VARCHAR2(50);
    IMPUESTO_IMPORTE VARCHAR2(50);
    SUMA_ASEGURADA  NUMBER;
    VALOR VARCHAR2(50);
    ACCDET_CODIGO  VARCHAR2(50);
    ACCDET_DESCRIPCION  VARCHAR2(50);
    ACCDET_VALOR  VARCHAR2(50);
    URL_COTIZACION  VARCHAR2(50);
    begin 
    
    /*BUSCAR LOS DATOS, DE RTA, EN LAS TABLAS CORESPONDIENTES 

        EARNIX_DATOS
        EARNIX_ACCESORIOS
        EARNIX_PERIODOS
        EARNIX_COBERTURAS
        EARNIX_PLANES
    */
    
    
    /* RTA*/
    apex_json.initialize_clob_output;
    apex_json.open_object;                                                  --{
    apex_json.write('mensaje','success');
    apex_json.write('resultado','ok');
    
    apex_json.write('nroPresupuesto',NNRO_PRESUP);
    apex_json.write('nroOrden',NRO_ORDEN);
    apex_json.write('descripcionAuto',DESCRIPCION_AUTO);
    apex_json.open_array('planes');   --[
    apex_json.open_object;            --{ planes
        apex_json.write('codigo',PLANES_CODIGO);
        apex_json.write('debito',PLANES_DEBITO);
        apex_json.write('descripcion',PLANES_DESCRIPCION);
        apex_json.write('efectivo',PLANES_EFECTIVO);
        apex_json.write('franquicia',PLANES_FRANQUICIA);
        apex_json.write('franquiciarobo',PLANES_FRANQUICIA_ROBO);
        apex_json.write('inspeccionable',PLANES_INSPECCIONABLE);
        apex_json.write('prima',PLANES_PRIMA);
        apex_json.write('primaefectivo',PLANES_PRIMA_EFECTIVO);
        apex_json.write('conRecuperador',PLANES_CON_RECUPERADOR);
        
        apex_json.open_array('coberturas');   --[
        apex_json.open_object;               --{ coberturas
            apex_json.write('codigo',COBERT_CODIGO);
            apex_json.write('importe',COBERT_IMPORTE);
        apex_json.close_object;              --} coberturas
        apex_json.close_array;               --]
        
         apex_json.open_array('impuestos');   --[
        apex_json.open_object;               --{ impuestos
            apex_json.write('codigo',IMPUESTO_CODIGO);
            apex_json.write('importe',IMPUESTO_IMPORTE);
        apex_json.close_object;              --} impuestos
        apex_json.close_array;               --]
        
    apex_json.close_object;              --} planes
    apex_json.close_array;               --]
    
    apex_json.write('tipoVehiculo',vtipo_vehiculo);
    apex_json.write('sumaAsegurada',SUMA_ASEGURADA);
    apex_json.write('valor',VALOR);
    
    apex_json.open_array('accesoriosDetalle');   --[
    apex_json.open_object;                       --{ accesoriosDetalle
        apex_json.write('codigo',ACCDET_CODIGO);
        apex_json.write('descripcion',ACCDET_DESCRIPCION);
        apex_json.write('valor',ACCDET_VALOR);
    apex_json.close_object;                      --} accesoriosDetalle
    apex_json.close_array;                       --]
    
    
    apex_json.write('provinciaId',nprovincia);
    apex_json.write('provincia',vprovincia);
    apex_json.write('tipoVehiculoId',ntipo_vehiculo);
    apex_json.write('codClaseId',ncod_clase);
    apex_json.write('codMarca',vcod_marca_out);
    apex_json.write('importado',vmarca_importado);
    apex_json.write('urlCotizacion',URL_COTIZACION);
    apex_json.close_object;                                                 --}
    resultado := apex_json.get_clob_output;
    apex_json.free_output;

   
    
    end DEVOLVER_COTIZACION;
--------------------------------------------------------------------------------                                  
PROCEDURE get_exception(
                      p_error_code IN VARCHAR,
                      p_excep OUT CLOB)
  AS
    l_grupo_notif varchar2(100):=null;
  BEGIN
    IF p_error_code = 'general' THEN
      apex_json.initialize_clob_output;
        apex_json.open_object; --{
        apex_json.write('Error_Code',sqlcode);
        apex_json.write('Error_Message',sqlerrm);
        apex_json.write('Location','Cargar Lote');
        apex_json.close_object;
      p_excep         :=apex_json.get_clob_output;
      
    elsif p_error_code = 'error_cotizador' THEN
      apex_json.initialize_clob_output;
      apex_json.open_object; --{
      apex_json.WRITE('Error_Code', p_error_code);
      apex_json.WRITE('Error_Message', 'Formato de campo Lote Aca no valido');
      apex_json.WRITE('Location', 'Cargar Lote');
      apex_json.WRITE('Runtime', sqlerrm);
      apex_json.close_object;
      p_excep         :=apex_json.get_clob_output;
      
    elsif p_error_code = 'test' THEN
      apex_json.initialize_clob_output;
      apex_json.open_object; --{
      apex_json.WRITE('Error_Code', p_error_code);
      apex_json.WRITE('Error_Message', 'Probando');
      apex_json.WRITE('Location', 'Cargar Lote');
      apex_json.WRITE('Runtime', 'probando');
      apex_json.close_object;
      p_excep         :=apex_json.get_clob_output; 
    END IF;
  END get_exception;

/*****************************************************************************************************/
END PCK_AUTOS_EMISION;
